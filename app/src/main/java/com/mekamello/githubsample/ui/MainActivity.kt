package com.mekamello.githubsample.ui

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.mekamello.githubsample.R
import com.mekamello.githubsample.domain.models.Repo
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
class MainActivity : AppCompatActivity(), MainView {

    @Inject
    lateinit var presenter: MainPresenter

    lateinit var adapter: RepoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setTitle(R.string.main_title)

        presenter.onCreate()

        adapter = RepoAdapter(object : RepoAdapter.Listener {
            override fun onItemClicked(repoId: Int) {
                presenter.onRepoDetailed(repoId)
            }
        })

        val layoutManager = LinearLayoutManager(this)
        val dividerDecoration = DividerItemDecoration(this, layoutManager.orientation)

        list.adapter = adapter
        list.layoutManager = layoutManager
        list.addItemDecoration(dividerDecoration)
        list.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val position = layoutManager.findLastVisibleItemPosition()

                adapter.getIdSinceLoadMore(position)?.let { presenter.onLoadMoreSince(it) }
            }
        })

        refresher.setOnRefreshListener { presenter.onLoadContent() }
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()
    }

    override fun showRepos(list: List<Repo>) {
        adapter.updateRepos(list)
    }

    override fun hideRepos() {
        adapter.clearRepos()
    }

    override fun showReposPagination(loadMore: Boolean) {
        adapter.setPagination(loadMore)
    }

    override fun showRepoDetailed(fullName: String, description: String) {
        AlertDialog.Builder(this)
                .setCancelable(true)
                .setTitle(fullName)
                .setMessage(description)
                .setPositiveButton(R.string.ok, { dialog, _ -> dialog.dismiss() })
                .create()
                .show()
    }

    override fun showUnknownError() {
        Toast.makeText(this, R.string.unknown_error, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        refresher.isRefreshing = true
    }

    override fun hideLoading() {
        refresher.isRefreshing = false
    }
}