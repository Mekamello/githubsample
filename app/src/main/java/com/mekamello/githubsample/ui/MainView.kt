package com.mekamello.githubsample.ui

import com.mekamello.githubsample.domain.models.Repo

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
interface MainView {
    fun showRepos(list: List<Repo>)
    fun showReposPagination(loadMore: Boolean)
    fun showRepoDetailed(fullName: String, description: String)
    fun showUnknownError()

    fun hideRepos()

    fun showLoading()
    fun hideLoading()
}