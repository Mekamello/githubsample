package com.mekamello.githubsample.ui

import com.mekamello.githubsample.domain.repositories.RepoRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
class MainPresenterImpl(
        private val repoRepository: RepoRepository,
        private val mainView: MainView
) : MainPresenter {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate() {
        compositeDisposable.add(repoRepository.getRepositories()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { mainView.showRepos(it) },
                        { mainView.showUnknownError() }
                )
        )

        loadRepositories(0)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
    }

    override fun onRepoDetailed(repoId: Int) {
        compositeDisposable.add(
                repoRepository.getRepository(repoId)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { mainView.showRepoDetailed(it.fullName, it.description) },
                                { mainView.showUnknownError() }
                        )
        )
    }

    override fun onLoadContent() {
        mainView.showLoading()

        deleteRepositoriesAndReload()
    }

    override fun onLoadMoreSince(since: Int) {
        loadRepositories(since)
    }

    private fun deleteRepositoriesAndReload() {
        compositeDisposable.add(
                repoRepository.deleteRepositories()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    mainView.hideRepos()
                                    loadRepositories(0)
                                },
                                {
                                    mainView.hideLoading()
                                    mainView.showUnknownError()
                                }
                        )
        )
    }

    private fun loadRepositories(since: Int) {
        compositeDisposable.add(
                repoRepository.updateRepositories(since = since)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    mainView.hideLoading()
                                    mainView.showReposPagination(it)
                                },
                                {
                                    mainView.hideLoading()
                                    mainView.showUnknownError()
                                }
                        )
        )
    }
}