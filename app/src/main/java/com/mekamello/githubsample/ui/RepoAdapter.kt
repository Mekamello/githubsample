package com.mekamello.githubsample.ui

import android.support.v7.util.SortedList
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mekamello.githubsample.R
import com.mekamello.githubsample.domain.models.Repo

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
class RepoAdapter(private val listener: Listener) : RecyclerView.Adapter<RepoAdapter.ViewHolder>() {

    companion object {
        private const val TYPE_PAGINATION = 0
        private const val TYPE_ITEM = 1
    }

    private val data: SortedList<Repo> = SortedList(Repo::class.java, object : SortedList.Callback<Repo>() {

        override fun compare(o1: Repo, o2: Repo): Int = o1.compare(o2)

        override fun areItemsTheSame(item1: Repo?, item2: Repo?): Boolean =
                item1?.id == item2?.id

        override fun onChanged(position: Int, count: Int) {
            notifyItemRangeChanged(position, count)
        }

        override fun areContentsTheSame(oldItem: Repo?, newItem: Repo?): Boolean =
                oldItem == newItem

        override fun onInserted(position: Int, count: Int) {
            notifyItemRangeInserted(position, count)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            notifyItemMoved(fromPosition, toPosition)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(position, count)
        }

    })

    override fun getItemCount(): Int = data.size()

    override fun getItemViewType(position: Int): Int =
            if (data[position] == Repo.PAGINATION) {
                TYPE_PAGINATION
            } else {
                TYPE_ITEM
            }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder =
            when (viewType) {
                TYPE_PAGINATION -> {
                    val view = LayoutInflater.from(parent?.context)
                            .inflate(R.layout.view_repo_pagination, parent, false)

                    Pagination(view = view)
                }
                TYPE_ITEM -> {
                    val view = LayoutInflater.from(parent?.context)
                            .inflate(R.layout.view_repo_item, parent, false)

                    Item(view = view, listener = listener)
                }
                else -> throw IllegalStateException("Invalid view type")
            }


    fun updateRepos(list: List<Repo>) {
        data.addAll(list)
        setPagination(list.isNotEmpty())
    }

    fun clearRepos() {
        data.clear()
    }

    fun setPagination(loadMore: Boolean) {
        if (data.size() > 0) {
            if (loadMore) {
                data.add(Repo.PAGINATION)
            } else {
                data.remove(Repo.PAGINATION)
            }
        }
    }

    fun getIdSinceLoadMore(position: Int): Int? =
            if (data.size() > position
                    && position != SortedList.INVALID_POSITION
                    && data[position] == Repo.PAGINATION) {
                data[position - 1].id
            } else {
                null
            }


    abstract class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(repo: Repo)
    }

    class Pagination(view: View) : ViewHolder(view) {

        override fun bind(repo: Repo) {}
    }

    class Item(view: View, private val listener: Listener) : ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.name)

        override fun bind(repo: Repo) {
            name.text = repo.name

            itemView.setOnClickListener { listener.onItemClicked(repo.id) }
        }
    }

    interface Listener {
        fun onItemClicked(repoId: Int)
    }
}