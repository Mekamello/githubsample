package com.mekamello.githubsample.ui

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
interface MainPresenter {
    fun onCreate()
    fun onDestroy()

    fun onLoadContent()
    fun onLoadMoreSince(since: Int)
    fun onRepoDetailed(repoId: Int)
}