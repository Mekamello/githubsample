package com.mekamello.githubsample.domain.repositories

import com.mekamello.githubsample.domain.models.Repo
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
interface RepoRepository {
    fun getRepositories(): Flowable<List<Repo>>

    fun getRepository(repoId: Int): Single<Repo>

    fun updateRepositories(since: Int): Single<Boolean>

    fun deleteRepositories(): Completable
}