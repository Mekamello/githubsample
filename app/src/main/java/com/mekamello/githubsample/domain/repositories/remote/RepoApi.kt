package com.mekamello.githubsample.domain.repositories.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
interface RepoApi {

    @GET("/repositories")
    fun getRepositories(@Query("since") since: String): Single<List<RepoResp>>

}