package com.mekamello.githubsample.domain.repositories

import com.mekamello.githubsample.domain.models.Repo
import com.mekamello.githubsample.domain.repositories.cache.RepoDao
import com.mekamello.githubsample.domain.repositories.remote.RepoApi
import com.mekamello.githubsample.domain.repositories.remote.RepoResp
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
class RepoRepositoryImpl(
        val repoDao: RepoDao,
        val repoApi: RepoApi
) : RepoRepository {

    override fun getRepository(repoId: Int): Single<Repo> =
            repoDao.getRepository(repoId)
                    .map { Repo(it) }

    override fun getRepositories(): Flowable<List<Repo>> =
            repoDao.getRepositories()
                    .subscribeOn(Schedulers.io())
                    .map { it.map { Repo(it) } }

    override fun updateRepositories(since: Int): Single<Boolean> =
            repoApi.getRepositories(since = since.toString())
                    .subscribeOn(Schedulers.io())
                    .flatMap { saveRepositoriesAndReturnIsLoadMore(it) }

    override fun deleteRepositories(): Completable =
            Completable.fromAction { repoDao.deleteAll() }
                    .subscribeOn(Schedulers.io())

    private fun saveRepositoriesAndReturnIsLoadMore(list: List<RepoResp>): Single<Boolean> =
            Completable.fromAction { repoDao.insertOrUpdate(list.map { it.toCache() }) }
                    .andThen(Single.just(list.isNotEmpty()))
}