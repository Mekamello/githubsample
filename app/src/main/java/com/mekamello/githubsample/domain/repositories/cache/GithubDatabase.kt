package com.mekamello.githubsample.domain.repositories.cache

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
@Database(
        entities = arrayOf(RepoData::class),
        version = 1
)
abstract class GithubDatabase : RoomDatabase() {

    companion object {
        private const val DATABASE_NAME = "db"

        fun createPersistenceDatabase(context: Context): GithubDatabase =
                Room.databaseBuilder(context.applicationContext, GithubDatabase::class.java, DATABASE_NAME)
                        .allowMainThreadQueries()
                        .build()
    }

    abstract fun getRepoDao(): RepoDao
}