package com.mekamello.githubsample.domain.repositories.cache

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
@Dao
interface RepoDao {

    @Query("SELECT * FROM repositories")
    fun getRepositories(): Flowable<List<RepoData>>

    @Query("SELECT * FROM repositories WHERE id = :repoId")
    fun getRepository(repoId:Int): Single<RepoData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(list: List<RepoData>)

    @Query("DELETE FROM repositories")
    fun deleteAll()
}