package com.mekamello.githubsample.domain.models

import com.mekamello.githubsample.domain.repositories.cache.RepoData

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
data class Repo(
        val id: Int,
        val name: String,
        val fullName: String,
        val description: String
) {

    constructor(data: RepoData) : this(
            id = data.id,
            name = data.name,
            fullName = data.fullName,
            description = data.description
    )

    companion object {
        val PAGINATION = Repo(-1, "", "", "")
    }

    fun compare(newOne: Repo): Int{
        if (this == PAGINATION){
            return 1
        }

        if (id < newOne.id) {
            return -1
        }

        if (id > newOne.id) {

            if (newOne == PAGINATION){
                return -1
            }

            return 1
        }

        return 0
    }
}