package com.mekamello.githubsample.domain.repositories.remote

import com.google.gson.annotations.SerializedName
import com.mekamello.githubsample.domain.repositories.cache.RepoData
import com.mekamello.githubsample.domain.repositories.cache.ToCache

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
data class RepoResp(
        @SerializedName("id") val id: Int?,
        @SerializedName("name") val name: String?,
        @SerializedName("full_name") val fullName: String?,
        @SerializedName("description") val description: String?
) : ToCache<RepoData> {

    override fun toCache(): RepoData = RepoData(
            id = id ?: 0,
            name = name ?: "",
            fullName = fullName ?: "",
            description = description ?: ""
    )
}