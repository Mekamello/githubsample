package com.mekamello.githubsample.domain.repositories.cache

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
@Entity(tableName = "repositories")
class RepoData(
       @PrimaryKey(autoGenerate = false) val id: Int,
       @ColumnInfo(name = "name") val name: String,
       @ColumnInfo(name = "full_name") val fullName: String,
       @ColumnInfo(name = "description") val description: String
)