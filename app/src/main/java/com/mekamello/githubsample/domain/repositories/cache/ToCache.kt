package com.mekamello.githubsample.domain.repositories.cache

/**
 * Project name GithubSample
 * Created by kkruchinin on 06.11.17.
 */
interface ToCache<out Entity> {
    fun toCache(): Entity
}