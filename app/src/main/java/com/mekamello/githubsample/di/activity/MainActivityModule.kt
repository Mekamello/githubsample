package com.mekamello.githubsample.di.activity

import com.mekamello.githubsample.domain.repositories.RepoRepository
import com.mekamello.githubsample.domain.repositories.RepoRepositoryImpl
import com.mekamello.githubsample.domain.repositories.cache.GithubDatabase
import com.mekamello.githubsample.domain.repositories.remote.RepoApi
import com.mekamello.githubsample.ui.MainActivity
import com.mekamello.githubsample.ui.MainPresenter
import com.mekamello.githubsample.ui.MainPresenterImpl
import com.mekamello.githubsample.ui.MainView
import dagger.Module
import dagger.Provides

/**
 * Project name GithubSample
 * Created by kkruchinin on 07.11.17.
 */
@Module
class MainActivityModule {

    @PerActivity
    @Provides
    fun provideMainView(activity: MainActivity): MainView = activity

    @PerActivity
    @Provides
    fun provideMainPresenter(repoRepository: RepoRepository, mainView: MainView): MainPresenter =
            MainPresenterImpl(repoRepository, mainView)

    @PerActivity
    @Provides
    fun provideRepoRepository(database: GithubDatabase, repoApi: RepoApi): RepoRepository =
            RepoRepositoryImpl(database.getRepoDao(), repoApi)
}