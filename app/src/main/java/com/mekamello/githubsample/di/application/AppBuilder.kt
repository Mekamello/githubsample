package com.mekamello.githubsample.di.application

import com.mekamello.githubsample.di.activity.MainActivityModule
import com.mekamello.githubsample.di.activity.PerActivity
import com.mekamello.githubsample.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Project name GithubSample
 * Created by kkruchinin on 07.11.17.
 */
@Module(includes = arrayOf(AppModule::class))
interface AppBuilder {

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class))
    fun contributeMainActivity(): MainActivity

}