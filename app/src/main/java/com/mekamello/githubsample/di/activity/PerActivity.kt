package com.mekamello.githubsample.di.activity

import javax.inject.Scope

/**
 * Project name GithubSample
 * Created by kkruchinin on 07.11.17.
 */
@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class PerActivity