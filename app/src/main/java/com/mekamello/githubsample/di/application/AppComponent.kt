package com.mekamello.githubsample.di.application

import com.mekamello.githubsample.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Project name GithubSample
 * Created by kkruchinin on 07.11.17.
 */
@Singleton
@Component(
        modules = arrayOf(
                AndroidInjectionModule::class,
                AppBuilder::class
        )
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun application(application: App): Builder
    }

    fun inject(application: App)

}