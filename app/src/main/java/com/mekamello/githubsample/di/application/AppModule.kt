package com.mekamello.githubsample.di.application

import android.content.Context
import com.mekamello.githubsample.App
import com.mekamello.githubsample.domain.repositories.cache.GithubDatabase
import com.mekamello.githubsample.domain.repositories.remote.RepoApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Project name GithubSample
 * Created by kkruchinin on 07.11.17.
 */
@Module(includes = arrayOf(RetrofitModule::class))
class AppModule {

    @Provides
    @Singleton
    fun provideApplication(application: App): Context = application

    @Provides
    @Singleton
    fun provideDatabase(context: Context): GithubDatabase =
            GithubDatabase.createPersistenceDatabase(context)

    @Provides
    @Singleton
    fun provideRepoApi(retrofit: Retrofit): RepoApi =
            retrofit.create(RepoApi::class.java)

}